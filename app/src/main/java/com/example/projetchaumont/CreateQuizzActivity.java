package com.example.projetchaumont;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateQuizzActivity extends AppCompatActivity {
    public static final String QUIZZ_NAME = "com.example.projetchaumont.QUIZZ_NAME";
    public static final String QUIZZ_NB_QUESTION = "com.example.projetchaumont.QUIZZ_NB_QUESTION";
    private static final String MDP = "MDP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quizz);

        buildAndShowAlertDialog();
    }

    private void buildAndShowAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.alertDialogTitle);
        builder.setCancelable(false);

        // Get the layout inflater, inflate customLayout & set it to dialog
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View prompt = inflater.inflate(R.layout.dialog_login, null);
        builder.setView(prompt);

        // Set cancel and submit buttons
        builder.setPositiveButton(R.string.okButton, null);
        builder.setNegativeButton(R.string.cancelButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        // Create dialog
        AlertDialog dialog = builder.create();

        // Edit onClickListener of submit alertDialog button
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText passwordEdit = prompt.findViewById(R.id.alertDialogPassword);
                        if (passwordEdit.getText().toString().equals(MDP)) {
                            Toast.makeText(CreateQuizzActivity.this,
                                    "MDP correct, accès autorisé",
                                    Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(CreateQuizzActivity.this,
                                    "MDP incorrect, accès refusé",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        // Show on current view
        dialog.show();
    }

    public void launchCreateQuestionActivity(View view) {
        EditText quizzNameEdit = findViewById(R.id.quizzNameEdit);
        EditText questionNumberEdit = findViewById(R.id.questionNumberEdit);

        if (Utils.isEmpty(quizzNameEdit) || Utils.isEmpty(questionNumberEdit)) {
            Toast.makeText(CreateQuizzActivity.this,
                    "Erreur: les 2 champs doivent être remplis !",
                    Toast.LENGTH_SHORT).show();
        } else if (Integer.parseInt(questionNumberEdit.getText().toString()) == 0) {
            Toast.makeText(CreateQuizzActivity.this,
                    "Erreur: il ne peut pas y avoir 0 questions !",
                    Toast.LENGTH_SHORT).show();
        } else {
            String quizzName = quizzNameEdit.getText().toString();
            int questionNumber = Integer.parseInt(questionNumberEdit.getText().toString());

            Intent intent = new Intent(CreateQuizzActivity.this, CreateQuestionActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(QUIZZ_NAME, quizzName);
            intent.putExtra(QUIZZ_NB_QUESTION, questionNumber);
            startActivity(intent);
        }
    }

    public void stopper(View view) {
        Toast.makeText(CreateQuizzActivity.this,
                "Création de questionnaire abandonnée",
                Toast.LENGTH_SHORT).show();
        finish();
    }
}