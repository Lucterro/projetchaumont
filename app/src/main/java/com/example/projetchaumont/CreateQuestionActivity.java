package com.example.projetchaumont;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateQuestionActivity extends AppCompatActivity {
    private Questionnaire questionnaire;
    List<Question> questions;
    private int currentQuestionIndex;
    private int quizzQuestionNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_question);

        // Get Intent data
        Intent intent = getIntent();
        String quizzName = intent.getStringExtra(CreateQuizzActivity.QUIZZ_NAME);
        quizzQuestionNumber = intent.getIntExtra(CreateQuizzActivity.QUIZZ_NB_QUESTION, 2);

        questions = new ArrayList<>();
        currentQuestionIndex = 0;
        questionnaire = new Questionnaire(quizzName, 0, false, null);

        displayQuestionNumber();
    }

    private void displayQuestionNumber() {
        TextView questionNumberText = findViewById(R.id.createQuestionText);
        questionNumberText.setText(getString(R.string.createQuestionText,
                currentQuestionIndex + 1, quizzQuestionNumber));
    }

    public void stopper(View view) {
        /*Toast.makeText(CreateQuestionActivity.this,
                "Création de questionnaire abandonnée",
                Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(CreateQuestionActivity.this, MainActivity.class);
        startActivity(intent);*/
        Toast.makeText(CreateQuestionActivity.this,
                "Création de question abandonnée",
                Toast.LENGTH_SHORT).show();
        finish();
    }

    private int getRightAnswer(RadioGroup rightAnswerRadioGroup) {
        int rightAnswer = 0;
        // Get selected radio button from radioGroup
        int selectedId = rightAnswerRadioGroup.getCheckedRadioButtonId();
        // Find the radiobutton by returned id
        RadioButton rightAnswerRadioButton = findViewById(selectedId);
        if (rightAnswerRadioButton.getText().equals("Réponse 1")) {
            rightAnswer = 0;
        } else if (rightAnswerRadioButton.getText().equals("Réponse 2")) {
            rightAnswer = 1;
        } else if (rightAnswerRadioButton.getText().equals("Réponse 3"))
            rightAnswer = 2;

        return rightAnswer;
    }

    public void createNextQuestion(View view) {
        EditText createQuestionEdit = findViewById(R.id.createQuestionEdit);
        EditText answer1Edit = findViewById(R.id.answer1Edit);
        EditText answer2Edit = findViewById(R.id.answer2Edit);
        EditText answer3Edit = findViewById(R.id.answer3Edit);
        RadioGroup rightAnswerRadioGroup = findViewById(R.id.rightAnswerRadioGroup);
        Question question;

        // If an input is empty
        if (Utils.isEmpty(createQuestionEdit) || Utils.isEmpty(answer1Edit)
                || Utils.isEmpty(answer2Edit) || Utils.isEmpty(answer3Edit)) {
            Toast.makeText(CreateQuestionActivity.this,
                    "Erreur: Tous les champs doivent être remplis !",
                    Toast.LENGTH_SHORT).show();

        } else if (rightAnswerRadioGroup.getCheckedRadioButtonId() == -1) {
            // Else if no radioButton checked, send error Toast
            Toast.makeText(CreateQuestionActivity.this,
                    "Erreur: Sélectionnez un bonne réponse !",
                    Toast.LENGTH_SHORT).show();

        } else if ((currentQuestionIndex + 1) % quizzQuestionNumber != 0) {
            // Get question data, create question, add it to questions list
            question = getQuestion(createQuestionEdit, answer1Edit, answer2Edit, answer3Edit, rightAnswerRadioGroup);
            questions.add(question);

            // Update question index & number
            currentQuestionIndex += 1;
            displayQuestionNumber();

            // Clear EditTexts & radioGroup
            createQuestionEdit.setText("");
            answer1Edit.setText("");
            answer2Edit.setText("");
            answer3Edit.setText("");
            rightAnswerRadioGroup.clearCheck();

        } else {
            Toast.makeText(CreateQuestionActivity.this,
                    "Création de questionnaire terminée !",
                    Toast.LENGTH_SHORT).show();
            // Get & add last question
            question = getQuestion(createQuestionEdit, answer1Edit, answer2Edit, answer3Edit, rightAnswerRadioGroup);
            questions.add(question);
            // Set questions to quizz, save file & finish activity
            questionnaire.setQuestions(questions);
            Utils.setJsonFile(getApplicationContext(), questionnaire);
            Intent intent = new Intent(CreateQuestionActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    // Get all data needed from view to create the question
    private Question getQuestion(EditText createQuestionEdit, EditText answer1Edit, EditText answer2Edit,
                                 EditText answer3Edit, RadioGroup rightAnswerRadioGroup) {
        String question = createQuestionEdit.getText().toString(); // Get question
        List<String> answers = Arrays.asList(answer1Edit.getText().toString(),
                answer2Edit.getText().toString(), answer3Edit.getText().toString()); // Get answer list
        int rightAnswer = getRightAnswer(rightAnswerRadioGroup); // Get right answer

        return new Question(question, answers, rightAnswer);
    }
}