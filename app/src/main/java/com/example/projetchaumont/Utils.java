package com.example.projetchaumont;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.widget.EditText;

import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class Utils {

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    static Questionnaire getJsonFile(Context context, String fileName) {
        String jsonString = null;

        try {
            FileInputStream fis;
            if (fileName.contains(".json")) {
                fis = context.openFileInput(fileName);
            } else {
                fis = context.openFileInput(fileName + ".json");
            }

            int size = fis.available();
            byte[] buffer = new byte[size];
            fis.read(buffer);
            fis.close();

            jsonString = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Gson gson = new Gson();
        return gson.fromJson(jsonString, Questionnaire.class);
    }

    static void setJsonFile(Context context, Questionnaire questionnaire) {
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            FileOutputStream fos = context.openFileOutput(questionnaire.getTitle() + ".json", Context.MODE_PRIVATE);
            fos.write(gson.toJson(questionnaire).getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static boolean isEmpty(EditText editText) {
        return editText.getText().toString().trim().length() <= 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static ArrayList<Questionnaire> getAllQuizz(Activity activity) {
        ArrayList<Questionnaire> quizzNames = new ArrayList<>();

        String[] fileList = activity.fileList();
        for (String fileName : fileList) {
            System.out.println("Nom fichier trouvé: " + fileName);
            quizzNames.add(Utils.getJsonFile(activity.getApplicationContext(), fileName));
        }
        return quizzNames;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static ArrayList<String> getQuizzNames(Activity activity) {
        ArrayList<String> quizzNames = new ArrayList<>();

        String[] fileList = activity.fileList();
        for (String fileName : fileList) {
            System.out.println("Nom fichier trouvé: " + fileName);
            Questionnaire questionnaire = Utils.getJsonFile(activity.getApplicationContext(), fileName);
            quizzNames.add(questionnaire.getTitle());
        }
        return quizzNames;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static ArrayList<Integer> getQuizzScores(Activity activity) {
        ArrayList<Integer> quizzNames = new ArrayList<>();

        String[] fileList = activity.fileList();
        for (String fileName : fileList) {
            System.out.println("Nom fichier trouvé: " + fileName);
            Questionnaire questionnaire = Utils.getJsonFile(activity.getApplicationContext(), fileName);
            quizzNames.add(questionnaire.getScore());
        }
        return quizzNames;
    }
}
