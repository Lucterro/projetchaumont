package com.example.projetchaumont;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Collections;
import java.util.List;

public class QuizzActivity extends AppCompatActivity {
    private Questionnaire questionnaire;
    private List<Question> questionList;
    private int currentQuestionIndex;
    private int score;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        Intent intent = getIntent();
        String quizzTitle = intent.getStringExtra(MainActivity.QUIZZ_TITLE);
        System.out.println("Titre reçu: " + quizzTitle);

        questionnaire = Utils.getJsonFile(getApplicationContext(), quizzTitle);
        this.initializeQuizz(questionnaire);
    }

    private void initializeQuizz(Questionnaire questionnaire) {
        currentQuestionIndex = 0;

        questionList = questionnaire.getQuestions();
        Collections.shuffle(questionList); // Order randomization

        // Setting display with 1st question content
        TextView quizzTitle = findViewById(R.id.quizzTitle);
        quizzTitle.setText(questionnaire.getTitle());

        displayQuestion(currentQuestionIndex);
    }

    private void displayQuestion(int currentQuestionIndex) {
        TextView questionNumberText = findViewById(R.id.questionNumberText);
        TextView questionText = findViewById(R.id.questionText);
        RadioGroup answers = findViewById(R.id.answers);
        RadioButton answer1 = findViewById(R.id.answer1);
        RadioButton answer2 = findViewById(R.id.answer2);
        RadioButton answer3 = findViewById(R.id.answer3);

        questionNumberText.setText(getString(R.string.questionNumber, currentQuestionIndex + 1,
                questionnaire.getQuestions().size()));
        questionText.setText(questionList.get(currentQuestionIndex).getQuestion());
        answer1.setText(questionList.get(currentQuestionIndex).getAnswers().get(0));
        answer2.setText(questionList.get(currentQuestionIndex).getAnswers().get(1));
        answer3.setText(questionList.get(currentQuestionIndex).getAnswers().get(2));
        answers.clearCheck();
    }

    public void nextQuestion(View view) {
        if ((currentQuestionIndex + 1) % questionList.size() != 0) {
            if (isRightAnswer()) {
                Toast.makeText(getApplicationContext(), "Bonne réponse !",
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Mauvaise réponse !",
                        Toast.LENGTH_SHORT).show();
            }
            currentQuestionIndex += 1;
            displayQuestion(currentQuestionIndex);
        } else {
            isRightAnswer();

            Toast.makeText(QuizzActivity.this,
                    "Questionnaire " + questionnaire.getTitle() + " fini ! Score= " + score,
                    Toast.LENGTH_SHORT).show();

            // Save score in file & end activity
            questionnaire.setScore(score);
            questionnaire.setDone(true);
            Utils.setJsonFile(getApplicationContext(), questionnaire);
            finish();
        }
    }

    private boolean isRightAnswer() {
        RadioGroup answers = findViewById(R.id.answers);
        RadioButton answer1 = findViewById(R.id.answer1);
        RadioButton answer2 = findViewById(R.id.answer2);
        RadioButton answer3 = findViewById(R.id.answer3);

        int choiceId = answers.getCheckedRadioButtonId();
        RadioButton selectedRadioButton = findViewById(choiceId);

        int choiceIndex = 0;
        if (selectedRadioButton == answer1) choiceIndex = 0;
        if (selectedRadioButton == answer2) choiceIndex = 1;
        if (selectedRadioButton == answer3) choiceIndex = 2;

        // Get selected radioButton id from radioGroup & compare it to rightAnswer id
        System.out.println("Réponse mise: " + choiceIndex);
        System.out.println("Vraie réponse: " + questionList.get(currentQuestionIndex).getRightAnswer());

        boolean right;
        if (choiceIndex == questionList.get(currentQuestionIndex).getRightAnswer()) {
            score += 1;
            right = true;
        } else {
            right = false;
        }

        return right;
    }

    public void stopper(View view) {
        Toast.makeText(QuizzActivity.this,
                "Questionnaire " + questionnaire.getTitle() + " abandonné",
                Toast.LENGTH_SHORT).show();
        finish();
    }
}