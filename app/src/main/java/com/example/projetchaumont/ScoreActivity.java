package com.example.projetchaumont;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScoreActivity extends AppCompatActivity {
    ArrayList<Questionnaire> listQuizz;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        final ListView scoreList = findViewById(R.id.scoreList);
        final TextView averageText = findViewById(R.id.averageScoreText);

        // Get all quizz
        listQuizz = Utils.getAllQuizz(this);

        // Set adapter for display
        scoreList.setAdapter(getAdapter());

        // Get & display grades average
        averageText.setText(getString(R.string.averageText, getAverage()));
    }

    private SimpleAdapter getAdapter() {
        List<Map<String, String>> dataList = new ArrayList<>();
        Map<String, String> score;

        // For every quizz, get the String resource filled with title, score & question count
        // Then put Map of 2 Strings with appropriate keys into the map list
        // Finally, create & return a simple adapter for display
        for (Questionnaire questionnaire : listQuizz) {
            score = new HashMap<>();
            String quizzName = getString(R.string.gradeText, questionnaire.getTitle());
            String quizzScore = getString(R.string.gradeScore, questionnaire.getScore(), questionnaire.getQuestions().size());
            score.put("1st line", quizzName);
            score.put("2nd line", quizzScore);
            dataList.add(score);
        }

        return new SimpleAdapter(this, dataList,
                android.R.layout.simple_list_item_2,
                new String[]{"1st line", "2nd line"},
                new int[]{android.R.id.text1, android.R.id.text2});
    }

    // Returns grade average for all quizz
    private int getAverage() {
        ArrayList<Integer> gradesOutOf20 = new ArrayList<>();

        // For every quizz, calculate the grade out of 20 & add it to the grade list
        for (Questionnaire questionnaire : listQuizz) {
            int gradeOutOf20 = questionnaire.getScore() * 20 / questionnaire.getQuestions().size();
            System.out.println("Note de " + questionnaire.getTitle() + " (" + questionnaire.getScore() + "/" + questionnaire.getQuestions().size() + ") sur 20: " + gradeOutOf20);
            gradesOutOf20.add(gradeOutOf20);
        }

        // Divide sum of all grades by quizz count to obtain average
        int average = this.sum(gradesOutOf20) / listQuizz.size();
        System.out.println("Moyenne totale: " + average);
        return average;
    }

    // Returns sum of grades in gradeList
    private int sum(List<Integer> list) {
        int sum = 0;

        for (int i : list)
            sum = sum + i;
        return sum;
    }

    public void finishActivity(View view) {
        finish();
    }
}