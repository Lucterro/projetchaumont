package com.example.projetchaumont;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static final String QUIZZ_TITLE = "com.example.projetchaumont.QUIZZ_TITLE";
    public static final String QUIZZ_LIST = "com.example.projetchaumont.QUIZZ_LIST";
    ArrayList<String> listQuizz;
    ArrayList<String> listFiles;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Assets can't be edited, I copy quizz to internal storage on 1st launch
        // Get list of quizz from assets/quizz directory
        listFiles = new ArrayList<>();
        listAssetFiles("quizz");

        // Check if they exist in internal storage
        for (String fileName : listFiles) {
            // If not, copy them in internal storage
            if (!fileExist(fileName)) {
                copyAssetFile(fileName);
            }
        }

        // Get stored quizz & populate listView
        updateQuizzList();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_main);

        // Get stored quizz & update listView
        updateQuizzList();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void updateQuizzList() {
        // Get the JSON files (quizz) in internal storage
        listQuizz = new ArrayList<>(Utils.getQuizzNames(this));
        System.out.println("Liste des titres: " + listQuizz.toString());

        // Update quizz number
        TextView categoriesText = findViewById(R.id.listTitle);
        categoriesText.setText(getString(R.string.listTitle, listQuizz.size()));

        // Create & inflate listView
        final ListView listView = findViewById(R.id.listQuizz);

        ListAdapter customAdapter = new MyCustomAdapter(this, R.layout.custom_adapter, listQuizz);
        listView.setAdapter(customAdapter);

        /*adapter = new ArrayAdapter<>(this,
                android.R.layout.select_dialog_item, listQuizz);
        listView.setAdapter(adapter);*/

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object listItem = listView.getItemAtPosition(position);

                Toast.makeText(MainActivity.this,
                        "Questionnaire " + listItem.toString() + " lancé",
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(MainActivity.this, QuizzActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(QUIZZ_TITLE, listItem.toString());
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.checkScoreOption:
                launchScoreActivity();
                return true;
            case R.id.resetScoreOption:
                resetScores();
                return true;
            case R.id.createQuizzOption:
                launchCreateQuizzActivity();
                return true;
            case R.id.quitOption:
                quit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void launchScoreActivity() {
        Intent intent = new Intent(MainActivity.this, ScoreActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(QUIZZ_LIST, listQuizz); // faux
        startActivity(intent);
    }

    private void launchCreateQuizzActivity() {
        Intent intent = new Intent(MainActivity.this, CreateQuizzActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    // For every quizz, set score to 0 & done to false
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void resetScores() {
        for (String quizz : listQuizz) {
            Questionnaire questionnaire = Utils.getJsonFile(getApplicationContext(), quizz);
            questionnaire.setScore(0);
            questionnaire.setDone(false);
            Utils.setJsonFile(getApplicationContext(), questionnaire);
        }
        updateQuizzList();
        Toast.makeText(MainActivity.this, "Scores reset",
                Toast.LENGTH_SHORT).show();
    }

    private void quit() {
        this.finishAffinity();
    }

    private boolean fileExist(String fileName) {
        File file = getBaseContext().getFileStreamPath(fileName);
        return file.exists();
    }

    private boolean listAssetFiles(String path) {
        String[] list;
        try {
            list = getAssets().list(path);
            if (list.length > 0) {
                // This is a folder
                for (String file : list) {
                    if (!listAssetFiles(path + "/" + file))
                        return false;
                    else {
                        // This is a file
                        System.out.println("Adding: " + file);
                        listFiles.add(file);
                    }
                }
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    private void copyAssetFile(String fileName) {
        InputStream in;
        FileOutputStream fOut;
        AssetManager assetManager = getAssets();
        try {
            in = assetManager.open("quizz/" + fileName);
            fOut = openFileOutput(fileName, Context.MODE_PRIVATE);

            copyFile(in, fOut);

            in.close();
            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            Log.e("tag", "Failed to copy asset file: " + fileName, e);
        }
    }

    private void copyFile(InputStream in, FileOutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }
}