package com.example.projetchaumont;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

class MyCustomAdapter extends ArrayAdapter<String> {
    private final Context mContext;
    private final ArrayList<String> quizzList;

    public MyCustomAdapter(Context context, int resourceLayout, ArrayList<String> quizzList) {
        super(context, resourceLayout, quizzList);
        this.mContext = context;
        this.quizzList = quizzList;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = inflater.inflate(R.layout.custom_adapter, parent, false);
        }

        String p = getItem(position);
        if (p != null) {
            TextView textView = view.findViewById(R.id.quizzTitleItem);
            if (textView != null) {
                if (isDone(position)){
                    //textView.setTextColor(ContextCompat.getColor(mContext, R.color.colorHint));
                    String quizzTitle = quizzList.get(position) + " " + mContext.getString(R.string.quizzDone);
                    textView.setText(quizzTitle);
                } else {
                    textView.setText(quizzList.get(position));
                }
            }
        }
        return view;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean isEnabled(int position) {
        return !isDone(position);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private boolean isDone(int position){
        String quizzTitle = quizzList.get(position);
        Questionnaire questionnaire = Utils.getJsonFile(mContext, quizzTitle);
        return questionnaire.isDone();
    }
}