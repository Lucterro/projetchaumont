package com.example.projetchaumont;

import java.io.Serializable;
import java.util.List;

public class Questionnaire implements Serializable {

    private String title;
    private int score;
    private boolean done;
    private List<Question> questions;

    public Questionnaire(String title, int score, boolean done, List<Question> questions) {
        this.title = title;
        this.score = score;
        this.done = done;
        this.questions = questions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "Questionnaire{" +
                "title='" + title + '\'' +
                ", score=" + score +
                ", done=" + done +
                ", questions=" + questions +
                '}';
    }
}
